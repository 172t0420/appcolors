package com.appcolors1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_accesorios.*

class AccesoriosActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_accesorios)

        item_brochas.setOnClickListener {
            startActivity(Intent(this, BrochasActivity::class.java))
        }
        item_rodillos.setOnClickListener {
            startActivity(Intent(this, RodillosActivity::class.java))
        }
        item_espatulas.setOnClickListener {
            startActivity(Intent(this, EspatulasActivity::class.java))
        }
        item_cintas.setOnClickListener {
            startActivity(Intent(this, CintasActivity::class.java))
        }
    }
}