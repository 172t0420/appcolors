package com.appcolors1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_ingreso.*

class IngresoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Theme_AppColors1)

        super.onCreate(savedInstanceState)
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        setContentView(R.layout.activity_ingreso)

        btnIniciar.setOnClickListener {
            val usuario = txtUsu.text.toString()
            val contrasenia = txtPass.text.toString()

            if(usuario.isEmpty()){
               Toast.makeText(this,"Ingrese el usuario", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }else if(usuario != "Administrador"){
                Toast.makeText(this,"Usuario incorrecto", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if(contrasenia.isEmpty()){
                Toast.makeText(this,"Ingresa la contraseña", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }else if(contrasenia != "123456"){
                Toast.makeText(this,"Contraseña incorrecta", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }else{
                abrirMenu()
            }
        }

        btnCrear.setOnClickListener {
            abrirRegistrar()
        }


    }

    fun abrirRegistrar(){
        startActivity(Intent(this, RegistrarActivity::class.java))
    }

    fun abrirMenu(){
        startActivity(Intent(this,MenuActivity::class.java))
    }
}