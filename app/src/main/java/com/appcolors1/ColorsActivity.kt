package com.appcolors1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_colors.*

class ColorsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_colors)

        item_colores.setOnClickListener {
            startActivity(Intent(this, PaletaActivity::class.java))
        }
        item_familias.setOnClickListener {
            startActivity(Intent(this, FamiliasActivity::class.java))
        }
    }
}