package com.appcolors1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_familias.*

class FamiliasActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_familias)

        item_fam1.setOnClickListener {
            startActivity(Intent(this,OffwhitesActivity::class.java))
        }
        item_fam2.setOnClickListener {
            startActivity(Intent(this,AmarillosActivity::class.java))
        }
        item_fam3.setOnClickListener {
            startActivity(Intent(this,NaranjasActivity::class.java))
        }
        item_fam4.setOnClickListener {
            startActivity(Intent(this,RojosActivity::class.java))
        }
        item_fam5.setOnClickListener {
            startActivity(Intent(this,AzulesActivity::class.java))
        }
        item_fam6.setOnClickListener {
            startActivity(Intent(this,VioletasActivity::class.java))
        }
        item_fam7.setOnClickListener {
            startActivity(Intent(this,VerdesActivity::class.java))
        }
        item_fam8.setOnClickListener {
            startActivity(Intent(this,NeutrosActivity::class.java))
        }
        item_fam9.setOnClickListener {
            startActivity(Intent(this,MagentasActivity::class.java))
        }
        item_fam10.setOnClickListener {
            startActivity(Intent(this,TurquesasActivity::class.java))
        }
        item_fam11.setOnClickListener {
            startActivity(Intent(this,CitricosActivity::class.java))
        }
        item_fam12.setOnClickListener {
            startActivity(Intent(this,GrisesActivity::class.java))
        }
        item_fam13.setOnClickListener {
            startActivity(Intent(this,AcentosActivity::class.java))
        }
    }
}