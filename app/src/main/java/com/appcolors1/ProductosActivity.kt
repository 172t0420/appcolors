package com.appcolors1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_productos.*

class ProductosActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_productos)

        item_muros.setOnClickListener {
            startActivity(Intent(this, MurosActivity::class.java))
        }
        item_estructuras.setOnClickListener {
            startActivity(Intent(this, EstructurasActivity::class.java))
        }
        item_pisos.setOnClickListener {
            startActivity(Intent(this, PisosActivity::class.java))
        }
        item_muebles.setOnClickListener {
            startActivity(Intent(this, MueblesActivity::class.java))
        }
        item_puertas.setOnClickListener {
            startActivity(Intent(this, PuertasActivity::class.java))
        }
        item_azoteas.setOnClickListener {
            startActivity(Intent(this, AzoteasActivity::class.java))
        }
        item_accesorios.setOnClickListener {
            startActivity(Intent(this, AccesoriosActivity::class.java))
        }

    }
}