package com.appcolors1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.appcolors1.Fragmentos.*
import kotlinx.android.synthetic.main.activity_estructuras.*


class EstructurasActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_estructuras)

        item_estructura1.setOnClickListener {
            val fragmento1 = Estructura1Fragment()
            val manager = supportFragmentManager
            val transaction = manager.beginTransaction()

            transaction.replace(R.id.reemplazar, fragmento1)
            transaction.addToBackStack(null)
            transaction.commit()
        }
        item_estructura2.setOnClickListener {
            val fragmento1 = Estructura2Fragment()
            val manager = supportFragmentManager
            val transaction = manager.beginTransaction()

            transaction.replace(R.id.reemplazar, fragmento1)
            transaction.addToBackStack(null)
            transaction.commit()
        }
        item_estructura3.setOnClickListener {
            val fragmento1 = Estructura3Fragment()
            val manager = supportFragmentManager
            val transaction = manager.beginTransaction()

            transaction.replace(R.id.reemplazar, fragmento1)
            transaction.addToBackStack(null)
            transaction.commit()
        }
        item_estructura4.setOnClickListener {
            val fragmento1 = Estructura4Fragment()
            val manager = supportFragmentManager
            val transaction = manager.beginTransaction()

            transaction.replace(R.id.reemplazar, fragmento1)
            transaction.addToBackStack(null)
            transaction.commit()
        }
        item_estructura5.setOnClickListener {
            val fragmento1 = Estructura5Fragment()
            val manager = supportFragmentManager
            val transaction = manager.beginTransaction()

            transaction.replace(R.id.reemplazar, fragmento1)
            transaction.addToBackStack(null)
            transaction.commit()
        }
    }
}