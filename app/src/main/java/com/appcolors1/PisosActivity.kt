package com.appcolors1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.appcolors1.Fragmentos.*
import kotlinx.android.synthetic.main.activity_pisos.*

class PisosActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pisos)

        item_piso1.setOnClickListener {
            val fragmento1 = Piso1Fragment()
            val manager = supportFragmentManager
            val transaction = manager.beginTransaction()

            transaction.replace(R.id.reemplazar, fragmento1)
            transaction.addToBackStack(null)
            transaction.commit()
        }
        item_piso2.setOnClickListener {
            val fragmento1 = Piso2Fragment()
            val manager = supportFragmentManager
            val transaction = manager.beginTransaction()

            transaction.replace(R.id.reemplazar, fragmento1)
            transaction.addToBackStack(null)
            transaction.commit()
        }
        item_piso3.setOnClickListener {
            val fragmento1 = Piso3Fragment()
            val manager = supportFragmentManager
            val transaction = manager.beginTransaction()

            transaction.replace(R.id.reemplazar, fragmento1)
            transaction.addToBackStack(null)
            transaction.commit()
        }
        item_piso4.setOnClickListener {
            val fragmento1 = Piso4Fragment()
            val manager = supportFragmentManager
            val transaction = manager.beginTransaction()

            transaction.replace(R.id.reemplazar, fragmento1)
            transaction.addToBackStack(null)
            transaction.commit()
        }
        item_piso5.setOnClickListener {
            val fragmento1 = Piso5Fragment()
            val manager = supportFragmentManager
            val transaction = manager.beginTransaction()

            transaction.replace(R.id.reemplazar, fragmento1)
            transaction.addToBackStack(null)
            transaction.commit()
        }
    }
}