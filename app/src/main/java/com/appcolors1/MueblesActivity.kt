package com.appcolors1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.appcolors1.Fragmentos.Piso1Fragment
import kotlinx.android.synthetic.main.activity_muebles.*
import com.appcolors1.Fragmentos.*

class MueblesActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_muebles)

        item_mueble1.setOnClickListener {
            val fragmento1 = Mueble1Fragment()
            val manager = supportFragmentManager
            val transaction = manager.beginTransaction()

            transaction.replace(R.id.reemplazar, fragmento1)
            transaction.addToBackStack(null)
            transaction.commit()
        }
        item_mueble2.setOnClickListener {
            val fragmento1 = Mueble2Fragment()
            val manager = supportFragmentManager
            val transaction = manager.beginTransaction()

            transaction.replace(R.id.reemplazar, fragmento1)
            transaction.addToBackStack(null)
            transaction.commit()
        }
        item_mueble3.setOnClickListener {
            val fragmento1 = Mueble3Fragment()
            val manager = supportFragmentManager
            val transaction = manager.beginTransaction()

            transaction.replace(R.id.reemplazar, fragmento1)
            transaction.addToBackStack(null)
            transaction.commit()
        }
        item_mueble4.setOnClickListener {
            val fragmento1 = Mueble4Fragment()
            val manager = supportFragmentManager
            val transaction = manager.beginTransaction()

            transaction.replace(R.id.reemplazar, fragmento1)
            transaction.addToBackStack(null)
            transaction.commit()
        }
        item_mueble5.setOnClickListener {
            val fragmento1 = Mueble5Fragment()
            val manager = supportFragmentManager
            val transaction = manager.beginTransaction()

            transaction.replace(R.id.reemplazar, fragmento1)
            transaction.addToBackStack(null)
            transaction.commit()
        }
    }
}